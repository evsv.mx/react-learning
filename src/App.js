import './App.css';
import Header from "./header/Header";

function App() {
    return (
        <div className='wrapper'>
            <Header/>
            <Nav/>
            <Main/>
            <Footer/>
        </div>
    );
}

function Main() {
    return (
        <main>
            <div className="top-cover">
                <img
                    src="https://miuc.org/wp-content/uploads/2020/08/6-Reasons-why-you-should-learn-Programming-1280x720.png"/>
            </div>
            <div className="content">
                <div className="profile">
                    <div className="avatar">
                        <img
                            src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSM1cDnT1Q5ZrkfLfxiSgFvC2ZsjpngynJGvg&usqp=CAU"
                            alt=""/>
                    </div>
                    <div className="info">
                        <div className="name">
                            <h2>Andrew Salzhenic</h2>
                        </div>
                        <div className="dateBirthday">
                            <div>Date of birthday:</div>
                            <div>18-12-1995</div>
                        </div>
                        <div className="Education">
                            <div>Universe:</div>
                            <div>DONNTU - 2024</div>
                        </div>
                    </div>
                </div>
                <hr/>

                <div className="posts">
                    <div className="creator-post">
                        <textarea name="" id="new-post-area" cols="30" rows="10"></textarea>
                        <button id="add-post">Add</button>
                    </div>

                    <div className="post">
                        <div className="profile">
                            <img
                                src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSM1cDnT1Q5ZrkfLfxiSgFvC2ZsjpngynJGvg&usqp=CAU"
                                alt=""/>
                            <h5>Andrew Salzhenic</h5>
                        </div>
                        <div className="message">Для скругления уголков у элементов в CSS3 предназначено свойство
                            border-radius, значением которого выступает радиус закругления. Если взять квадратное
                            изображение и добавить к нему это свойство, то мы получим уже не квадратное, а круглое
                            изображение. В качестве значения следует задать половину ширины рисунка. Правда, можно
                            поступить и проще и значением указать заведомо большое число, превышающее размеры
                            изображения. Так мы в любом случае получим круглую картинку и сможем применять стиль к
                            изображениям разного размера.
                        </div>
                    </div>
                    <div className="post">
                        <div className="profile">
                            <img
                                src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSM1cDnT1Q5ZrkfLfxiSgFvC2ZsjpngynJGvg&usqp=CAU"
                                alt=""/>
                            <h5>Andrew Salzhenic</h5>
                        </div>
                        <div className="message">Для скругления уголков у элементов в CSS3 предназначено свойство
                            border-radius, значением которого выступает радиус закругления. Если взять квадратное
                            изображение и добавить к нему это свойство, то мы получим уже не квадратное, а круглое
                            изображение. В качестве значения следует задать половину ширины рисунка. Правда, можно
                            поступить и проще и значением указать заведомо большое число, превышающее размеры
                            изображения. Так мы в любом случае получим круглую картинку и сможем применять стиль к
                            изображениям разного размера.
                        </div>
                    </div>
                    <div className="post">
                        <div className="profile">
                            <img
                                src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSM1cDnT1Q5ZrkfLfxiSgFvC2ZsjpngynJGvg&usqp=CAU"
                                alt=""/>
                            <h5>Andrew Salzhenic</h5>
                        </div>
                        <div className="message">Для скругления уголков у элементов в CSS3 предназначено свойство
                            border-radius, значением которого выступает радиус закругления. Если взять квадратное
                            изображение и добавить к нему это свойство, то мы получим уже не квадратное, а круглое
                            изображение. В качестве значения следует задать половину ширины рисунка. Правда, можно
                            поступить и проще и значением указать заведомо большое число, превышающее размеры
                            изображения. Так мы в любом случае получим круглую картинку и сможем применять стиль к
                            изображениям разного размера.
                        </div>
                    </div>
                </div>
            </div>
        </main>
    );
}

function Nav() {
    return (
        <nav>
            <ul>
                <li>profile</li>
                <li>message</li>
                <li>news</li>
                <li>music</li>
                <li></li>
                <li>settings</li>
            </ul>
        </nav>
    );
}

function Footer() {
    return (
        <footer>
            <h2>Hello, I'm footer xD</h2>
        </footer>
    );
}

export default App;
