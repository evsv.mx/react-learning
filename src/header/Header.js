import logo from './../logo.svg'

function Header() {
    return (
        <header>
            <img src={logo} width={100}/>
            <h1>Social Network</h1>
        </header>
    );
}
export default Header;